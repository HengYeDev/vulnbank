## Vulnbank

This is a "vulnerable" application to demonstrate certain web security issues. These issues have been designed in such a
way that they do not require particular tools or knowledge to attack. Additionally, it is designed that it can be attacked
using only user inputs in a web browser, without the use of the Inspect Element, View Source, or any other tools. This may
sound unreasonable since tools are an important part of any security tester's knowledge. However, this application is
intended to be used to introduce students in a school setting to cybersecurity. Since schools often limit what tools students
may access, applications like this one are key to acquiring interest in cybersecurity. 

However, it is not intended that this application be an extremely realistic portrayal of cybersecurity. In both capture-the-flag
competitions and real world security testing, it is important to use tools. Also, this application is purely front-end. The reason
for this is that there should be no state management, so a reload of the page should allow the user to restart the application. It
is my intention that this make it easier to demonstrate it in educational environments.
The drawback is that a professional pentester should not even consider these challenges challenging, since you can simply use
"Inspect Element" to change anything on the front-end you desire, including looking through the javascript. 



